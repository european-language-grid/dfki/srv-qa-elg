# srv-qa-elg: Question-Answering system about ELG
QA-System that will answer questions about ELG. 
The sytem will respond with the sentence from the ELG Grant Agreement which is most similar to the user input 
(similar according to cosine similarity)

author: Ela Elsholz

date: July 2019

## Requirements
Docker (Desktop) installed, Development Version: 18.09.2

when run without Docker:

[Chatterbot](https://chatterbot.readthedocs.io/en/stable/)
```pip install chatterbot```
Chatterbot corpus 
```pip install chatterbot-corpus```

Pandas ```pip install pandas```


## Usage
```
docker build -t qa-elg your-file-path
docker run -ti qa-elg
```